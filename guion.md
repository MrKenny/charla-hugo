# Guión charla Hugo Framework

Hola!, soy Kenny estudiante de TI y miembro de SUGUS.



## Enlaces para documentarse

* Plantilla presentación:
https://www.slidescarnival.com/es/tag/tecnologico

* GIT/GITHUB/GITLAB (sistema de control de versiones)
https://git-scm.com/downloads
https://github.com/
https://gitlab.com/


* visual studio code:
https://code.visualstudio.com/download

* Hugo:
https://gohugo.io/getting-started/quick-start/
https://platzi.com/blog/hugo-golang-platzi/
https://themes.gohugo.io/

*Markdown (escribir html más fácil)
https://markdown.es/sintaxis-markdown/